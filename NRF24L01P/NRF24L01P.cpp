#include "NRF24L01P.h"

NRF24L01P::NRF24L01P(HardwareSpi *spi, Gpio *chipEnable)
    : _spi(spi), _chipEnable(chipEnable)
{
  // Configure spi speed at 10MHz
  _spi->setSpeed(10000000);

  // Disable RF chip
  disableRfChip();
}

NRF24L01P::~NRF24L01P()
{
  // Power down and disable the RF chip
  powerDown();
  disableRfChip();
}

void NRF24L01P::powerUp()
{
  // Set the power up bit
  writeRegister(CONFIG, readRegister(CONFIG) | PWR_UP);

  // Sleep for the max. power up time
  usleep(150);
}

void NRF24L01P::powerDown()
{
  // Clear the power up bit
  writeRegister(CONFIG, readRegister(CONFIG) & ~PWR_UP);
}

void NRF24L01P::enableRfChip()
{
  _chipEnable->set(high);
}

void NRF24L01P::disableRfChip()
{
  _chipEnable->set(low);
}

byte_q NRF24L01P::readRegister(Register reg)
{
  // Take the lower 5 bits to create a "read register" command
  // |0|0|0|A|A|A|A|A| --> 0x1F
  byte_q read = reg & 0x1F;

  // Tell the device what register we want to read from
  *_spi << read;
  byte_q *ret = _spi->transfer(1);
  byte_q value = ret[0];
  delete ret;

  return value;
}

void NRF24L01P::writeRegister(Register reg, byte_q value)
{
  // Take the lower 5 bits and set the 6. bit
  // to create a "write register" command
  // |0|0|1|A|A|A|A|A| --> 0x1F | 0x20
  byte_q write = (reg & 0x1F) | 0x20;

  // Tell the device what register we want to write to
  // and add the value
  *_spi << write << value;

  // Write
  _spi->transfer(0);
}

void NRF24L01P::setToRXMode()
{
  // Enable the chip and set the module into RX mode
  enableRfChip();
  writeRegister(CONFIG, readRegister(CONFIG) | PRIM_RX);

  // Sleep for the max. start up time
  usleep(130);
}
