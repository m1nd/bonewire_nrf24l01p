#ifndef NRF24L01P_H
#define NRF24L01P_H

//
// System include
//
#include <unistd.h>  // usleep()

//
// Project includes
//
#include "Bonewire/Bonewire.h"
#include "Bonewire/Gpio/Gpio.h"
#include "Bonewire/Spi/Hardware/HardwareSpi.h"

//
// ----------
//  Register
// ----------
//
enum Register
{
  CONFIG = 0x00,
  EN_AA = 0x01,
  EN_RXADDR = 0x02,
  SETUP_AW = 0x03,
  SETUP_RETR = 0x04,
  RF_CH = 0x05,
  RF_SETUP = 0x06,
  STATUS = 0x07,
  OBSERVE_TX = 0x08,
  RPD = 0x09,
  RX_ADDR_P0 = 0x0A,
  RX_ADDR_P1 = 0x0B,
  RX_ADDR_P2 = 0x0C,
  RX_ADDR_P3 = 0x0D,
  RX_ADDR_P4 = 0x0E,
  RX_ADDR_P5 = 0x0F,
  TX_ADDR = 0x10,
  RX_PW_P0 = 0x11,
  RX_PW_P1 = 0x12,
  RX_PW_P2 = 0x13,
  RX_PW_P3 = 0x14,
  RX_PW_P4 = 0x15,
  RX_PW_P5 = 0x16,
  FIFO_STATUS = 0x17,
  DYNPD = 0x1C,
  FEATURE = 0x1D
};

//
// ---------------------------
//  Config register addresses
// ---------------------------
//
enum ConfigRegisterAddress
{
  PRIM_RX = 0x01, PWR_UP = 0x02, CRCO = 0x04, EN_CRC = 0x08, MASK_MAX_RT = 0x10, MASK_TX_DS = 0x20, MASK_RX_DR = 0x40
};

//
// Using
//
using namespace Bonewire;

//
// Example usage
//
/*
 Gpio chipEnable(GPIO_30, output);
 HardwareSpi spi(SPI_1_0);

 NRF24L01P nrf24(spi, chipEnable);
 nrf24.powerUp();
 nrf24.writeRegister(EN_AA, 0x00);
 nrf24.writeRegister(RF_SETUP, 0x0E);

 while (true)
 {
 for (uint32_q i = 0; i < 126; i++)
 {
 nrf24.writeRegister(RF_CH, i);
 nrf24.setToRXMode();
 usleep(40);
 nrf24.disableRfChip();

 if (nrf24.readRegister(RPD) & 0x01)
 std::cout << 2400 + i << "MHz" << std::endl;
 }
 }
 */

class NRF24L01P
{
private:
  /**
   * SPI bus
   */
  HardwareSpi *_spi;

  /**
   * Chip enable gpio
   */
  Gpio *_chipEnable;

  /**
   * Copy constructor
   */
  NRF24L01P(NRF24L01P &original);

  /**
   * Assignment operator
   */
  NRF24L01P &operator=(const NRF24L01P &source);

public:
  /**
   * Overloaded constructor
   */
  NRF24L01P(HardwareSpi *spi, Gpio *chipEnable);

  /**
   * Default destructor
   */
  virtual ~NRF24L01P();

  /**
   * Power up the module.
   *
   * @return void
   */
  void powerUp();

  /**
   * Power down the module.
   *
   * @return void
   */
  void powerDown();

  /**
   * Enable the RF chip.
   *
   * @return void
   */
  void enableRfChip();

  /**
   * Disable the RF chip.
   *
   * @return void
   */
  void disableRfChip();

  /**
   * Read the value of the target register.
   *
   * @param reg Register
   * @return    Register value
   */
  byte_q readRegister(Register reg);

  /**
   * Write the value to the target register.
   *
   * @param reg    Register
   * @param value  Value
   * @return       void
   */
  void writeRegister(Register reg, byte_q value);

  /**
   * Set the module into RX mode.
   *
   * @return void
   */
  void setToRXMode();
};

#endif // NRF24L01P_H
